import React, { useState } from "react";
import Home from '../components/Home'
import '../styles/main.css'
import { lista } from '../assets/lista'

class App extends React.Component {
    constructor(props){
        super(props);  
        this.state={
            data:lista            
        }
        this.onInputchange = this.onInputchange.bind(this);
        this.onSubmitForm = this.onSubmitForm.bind(this);
    }
    onInputchange(event) {
        this.setState({
          [event.target.name]: event.target.value
        });
    }
    onSubmitForm() {     
         var valor=0
         valor=Number(this.state.name)                                      
         const result = lista.filter(student => student.beds === valor)
        console.warn(result)           
        if (result.length > 0) {
          const copy = [];  
          result.forEach(function (item) {
              copy.push(item);
          });
        this.setState({data:copy})         
        } else { 
        alert('No hemos encontrado registros con esa busqueda')       
        this.setState({data:lista})
        }        
    }
    componentDidMount(){
        this.setState({data:lista})        
    }                          
    render() {
      return (
        <div>
      <h1 className="Title">Datos</h1>
      <label>Nro Camas</label>      
      <input type="text" id="name" name="name" minlength="4" maxlength="8" size="10" value={this.state.name} onChange={this.onInputchange}/>
      <button onClick={this.onSubmitForm}>BUSCAR</button>      
      <div className="contenedor">
            <Home title={"Casas en Bolivia"} data={this.state.data} />
      </div>      
    </div>          
      )
    }
  }
export default App;